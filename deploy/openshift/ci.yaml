apiVersion: v1
kind: Template
labels:
  app: jenkins-agent-trufflehog
  template: jenkins-agent-trufflehog-jenkins-pipeline
metadata:
  annotations:
    description: Jenkins Agent TruffleHog Image - Jenkinsfile
      see https://gitlab.com/synacksynack/opsperator/docker-jenkins-agent-trufflehog
    iconClass: icon-openshift
    openshift.io/display-name: Jenkins Agent TruffleHog CI
    tags: jenkins-agent-trufflehog
  name: jenkins-agent-trufflehog-jenkins-pipeline
objects:
- apiVersion: v1
  kind: BuildConfig
  metadata:
    annotations:
      description: Builds Jenkins Agent TruffleHog images
    name: jenkinsagenttrufflehog-jenkins-pipeline
  spec:
    strategy:
      jenkinsPipelineStrategy:
        jenkinsfile: |-
          def gitCommitMsg = ''
          def templateMark = 'jah-jenkins-ci'
          def templateSel  = 'jenkins-ci-mark'
          pipeline {
              agent {
                  node { label 'maven' }
              }
              options { timeout(time: 130, unit: 'MINUTES') }
              parameters {
                  string(defaultValue: 'master', description: 'Jenkins Agent TruffleHog Docker Image - Source Git Branch', name: 'jenkinsagenttrufflehogBranch')
                  string(defaultValue: 'master', description: 'Jenkins Agent TruffleHog Docker Image - Source Git Hash', name: 'jenkinsagenttrufflehogHash')
                  string(defaultValue: '${GIT_SOURCE_HOST}/faust64/docker-jenkins-agent-trufflehog.git', description: 'Jenkins Agent TruffleHog Docker Image - Source Git Repository', name: 'jenkinsagenttrufflehogRepo')
                  string(defaultValue: '3', description: 'Max Retry', name: 'jobMaxRetry')
                  string(defaultValue: '1', description: 'Retry Count', name: 'jobRetryCount')
              }
              stages {
                  stage('pre-cleanup') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      echo "Using project: ${openshift.project()}"
                                      echo "cleaning up previous assets for jah-${params.jenkinsagenttrufflehogHash}"
                                      openshift.selector("all", [ "${templateSel}": "${templateMark}-${params.jenkinsagenttrufflehogHash}" ]).delete()
                                      openshift.selector("secrets", [ "${templateSel}": "${templateMark}-${params.jenkinsagenttrufflehogHash}" ]).delete()
                                  }
                              }
                          }
                      }
                  }
                  stage('create') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      def namespace = "${openshift.project()}"
                                      try {
                                          timeout(10) {
                                              def cloneProto = "http"
                                              def created
                                              def objectsFromTemplate
                                              def privateRepo = false
                                              def repoHost = params.jenkinsagenttrufflehogRepo.split('/')[0]
                                              def templatePath = "/tmp/workspace/${namespace}/${namespace}-jenkinsagenttrufflehog-jenkins-pipeline/tmpjah${params.jenkinsagenttrufflehogBranch}/openshift"
                                              sh "git config --global http.sslVerify false"
                                              sh "rm -fr tmpjah${params.jenkinsagenttrufflehogBranch}; mkdir -p tmpjah${params.jenkinsagenttrufflehogBranch}"
                                              dir ("tmpjah${params.jenkinsagenttrufflehogBranch}") {
                                                  try {
                                                      withCredentials([string(credentialsId: "git-${repoHost}", variable: 'GIT_TOKEN')]) {
                                                          cloneProto = "https"
                                                          privateRepo = true
                                                          echo "cloning ${params.jenkinsagenttrufflehogRepo} over https, using ${repoHost} token"
                                                          try { git([ branch: "${params.jenkinsagenttrufflehogBranch}", url: "https://${GIT_TOKEN}@${params.jenkinsagenttrufflehogRepo}" ]) }
                                                          catch(e2) {
                                                              echo "Failed cloning ${params.jenkinsagenttrufflehogRepo}#${params.jenkinsagenttrufflehogBranch} - ${e2}"
                                                              throw e2
                                                          }
                                                      }
                                                  } catch(e) {
                                                      if (privateRepo != true) {
                                                          echo "caught ${e} - assuming no credentials required"
                                                          echo "cloning ${params.jenkinsagenttrufflehogRepo} over http"
                                                          try { git([ branch: "${params.jenkinsagenttrufflehogBranch}", url: "http://${params.jenkinsagenttrufflehogRepo}" ]) }
                                                          catch(e2) {
                                                              echo "Failed cloning ${params.jenkinsagenttrufflehogRepo}#${params.jenkinsagenttrufflehogBranch} - ${e2}"
                                                              throw e2
                                                          }
                                                      } else { throw e }
                                                  }
                                                  try {
                                                      gitCommitMsg = sh(returnStdout: true, script: "git log -n 1").trim()
                                                  } catch(e) { echo "In non-critical catch block resolving commit message - ${e}" }
                                              }
                                              try { sh "test -d ${templatePath}" }
                                              catch (e) {
                                                  echo "Could not find ./openshift in ${params.jenkinsagenttrufflehogRepo}#${params.jenkinsagenttrufflehogBranch}"
                                                  throw e
                                              }
                                              echo "Processing JenkinsAgentTruffleHog:${params.jenkinsagenttrufflehogHash}, from ${repoHost}, tagging to ${params.jenkinsagenttrufflehogBranch}"
                                              try {
                                                  echo " == Creating ImageStream =="
                                                  objectsFromTemplate = openshift.process("-f", "${templatePath}/imagestream.yaml")
                                                  echo "The template will create ${objectsFromTemplate.size()} objects"
                                                  created = openshift.apply(objectsFromTemplate)
                                                  created.withEach { echo "Created ${it.name()} with labels ${it.object().metadata.labels}" }
                                              } catch(e) { echo "In non-critical catch block while creating ImageStream - ${e}" }
                                              echo " == Creating BuildConfigs =="
                                              if (privateRepo) {
                                                  withCredentials([string(credentialsId: "git-${repoHost}", variable: 'GIT_TOKEN')]) {
                                                      objectsFromTemplate = openshift.process("-f", "${templatePath}/build-with-secret.yaml", '-p', "GIT_DEPLOYMENT_TOKEN=${GIT_TOKEN}",
                                                          '-p', "JAT_REPOSITORY_REF=${params.jenkinsagenttrufflehogHash}", '-p', "JAT_REPOSITORY_URL=${cloneProto}://${params.jenkinsagenttrufflehogRepo}")
                                                  }
                                              } else {
                                                  objectsFromTemplate = openshift.process("-f", "${templatePath}/build.yaml",
                                                      '-p', "JAT_REPOSITORY_REF=${params.jenkinsagenttrufflehogHash}", '-p', "JAT_REPOSITORY_URL=${cloneProto}://${params.jenkinsagenttrufflehogRepo}")
                                              }
                                              echo "The template will create ${objectsFromTemplate.size()} objects"
                                              for (o in objectsFromTemplate) { o.metadata.labels["${templateSel}"] = "${templateMark}-${params.jenkinsagenttrufflehogHash}" }
                                              created = openshift.apply(objectsFromTemplate)
                                              created.withEach { echo "Created ${it.name()} from template with labels ${it.object().metadata.labels}" }
                                          }
                                      } catch(e) {
                                          echo "In catch block while creating resources - ${e}"
                                          throw e
                                      }
                                  }
                              }
                          }
                      }
                  }
                  stage('build') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      try {
                                          timeout(90) {
                                              echo "watching jah-${params.jenkinsagenttrufflehogHash} docker image build"
                                              def builds = openshift.selector("bc", [ name: "jah-${params.jenkinsagenttrufflehogHash}" ]).related('builds')
                                              builds.untilEach(1) { return (it.object().status.phase == "Complete") }
                                          }
                                      } catch(e) {
                                          echo "In catch block while building Docker image - ${e}"
                                          throw e
                                      }
                                  }
                              }
                          }
                      }
                  }
                  stage('tag') {
                      steps {
                          script {
                              if ("${params.jenkinsagenttrufflehogBranch}" == "${params.jenkinsagenttrufflehogHash}") { echo "skipping tag - source matches target" }
                              else {
                                  openshift.withCluster() {
                                      openshift.withProject() {
                                          try {
                                              timeout(5) {
                                                  def namespace = "${openshift.project()}"
                                                  retry(3) {
                                                      sh """
                                                      oc login https://kubernetes.default.svc.cluster.local --certificate-authority=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt --token=\$(cat /var/run/secrets/kubernetes.io/serviceaccount/token) > /dev/null 2>&1
                                                      oc describe -n ${namespace} imagestreamtag jenkins-agent-trufflehog:${params.jenkinsagenttrufflehogHash} || exit 1
                                                      oc tag -n ${namespace} jenkins-agent-trufflehog:${params.jenkinsagenttrufflehogHash} jenkins-agent-trufflehog:${params.jenkinsagenttrufflehogBranch}
                                                      """
                                                  }
                                              }
                                          } catch(e) {
                                              echo "In catch block while tagging Jenkins Agent TruffleHog image - ${e}"
                                              throw e
                                          }
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
              post {
                  always {
                      script {
                          openshift.withCluster() {
                              openshift.withProject() {
                                  def namespace   = "${openshift.project()}"
                                  def postJobName = "${namespace}/${namespace}-post-triggers-jenkins-pipeline"
                                  currentBuild.description = """
                                  ${params.jenkinsagenttrufflehogRepo} ${params.jenkinsagenttrufflehogBranch} (try ${params.jobRetryCount}/${params.jobMaxRetry})
                                  ${gitCommitMsg}
                                  """.stripIndent()
                                  echo "cleaning up assets for jah-${params.jenkinsagenttrufflehogHash}"
                                  sh "rm -fr /tmp/workspace/${namespace}/${namespace}-jenkinsagenttrufflehog-jenkins-pipeline/tmpjah${params.jenkinsagenttrufflehogBranch}"
                                  openshift.selector("all", [ "${templateSel}": "${templateMark}-${params.jenkinsagenttrufflehogHash}" ]).delete()
                                  openshift.selector("secrets", [ "${templateSel}": "${templateMark}-${params.jenkinsagenttrufflehogHash}" ]).delete()
                                  def jobParams = [
                                          [$class: 'StringParameterValue', name: "jobMaxRetry", value: params.jobMaxRetry],
                                          [$class: 'StringParameterValue', name: "jobRetryCount", value: params.jobRetryCount],
                                          [$class: 'StringParameterValue', name: "jobStatus", value: currentBuild.currentResult],
                                          [$class: 'StringParameterValue', name: "sourceBranch", value: params.jenkinsagenttrufflehogBranch],
                                          [$class: 'StringParameterValue', name: "sourceComponent", value: "jenkinsagenttrufflehog"],
                                          [$class: 'StringParameterValue', name: "sourceImageStream", value: "jenkins-agent-trufflehog"],
                                          [$class: 'StringParameterValue', name: "sourceRef", value: params.jenkinsagenttrufflehogHash],
                                          [$class: 'StringParameterValue', name: "sourceRepo", value: params.jenkinsagenttrufflehogRepo]
                                      ]
                                  try { build job: postJobName, parameters: jobParams, propagate: false, wait: false }
                                  catch(e) { echo "caught ${e} starting Job post-process" }
                              }
                          }
                      }
                  }
                  changed { echo "changed?" }
                  failure { echo "Build failed (${params.jobRetryCount} out of ${params.jobMaxRetry})" }
                  success { echo "success!" }
                  unstable { echo "unstable?" }
              }
          }
      type: JenkinsPipeline
parameters:
- name: GIT_SOURCE_HOST
  description: Git FQDN we would build images from
  displayName: Git
  value: gitlab.com
