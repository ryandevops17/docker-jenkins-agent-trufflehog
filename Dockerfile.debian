FROM docker.io/debian:buster-slim

LABEL com.redhat.component="tekton-agent-trufflehog-buster-container" \
      io.k8s.description="The tekton agent trufflehog image has the tools scanning Git repositories for secrets." \
      io.k8s.display-name="Tekton Agent - TruffleHog" \
      io.openshift.tags="openshift,tekton,agent,trufflehog" \
      name="ci/tekton-agent-trufflehog" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-jenkins-agent-trufflehog" \
      version="1.0.3"

ENV TH_REPO=https://github.com/trufflesecurity/trufflehog/releases/download \
    TH_VERSION=3.6.4

RUN set -x \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	apt-get -y upgrade; \
    fi \
    && apt-get -y install ca-certificates curl \
    && if test `uname -m` = aarch64; then \
	TH_ARCH=arm64; \
    else \
	TH_ARCH=amd64; \
    fi \
    && curl -o /tmp/trufflehog.tar.gz \
	-fsL "$TH_REPO/v$TH_VERSION/trufflehog_${TH_VERSION}_linux_$TH_ARCH.tar.gz" \
    && tar -C /usr/local/bin -xf /tmp/trufflehog.tar.gz trufflehog \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rf /usr/share/man /usr/share/doc /var/lib/apt/lists/* \
	/tmp/trufflehog.tar.gz \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy \
	TH_ARCH

USER 1001
